﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Kollection.Model.Entities
{
    public class Wargear
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Points { get; set; }
        [DisplayFormat(NullDisplayText = "http://placehold.it/300x300")]
        public string Image { get; set; }
        [Display(Name="Slot")]
        public int WargearSlotId { get; set; }
        [Display(Name="Slot")]
        public virtual WargearSlot WargearSlot { get; set; }
        public virtual List<UnitWargear> UnitWargears { get; set; }
        public virtual List<WargearProfile> Profiles { get; set; }
        
        [Display(Name = "STR")]
        public string Strength { get; set; }
        public string AP { get; set; }
        [Display(Name = "DMG")]
        public string Damage { get; set; }
        public string Range { get; set; }
        public string Type { get; set; }
        public string Abilities { get; set; }
    }
}

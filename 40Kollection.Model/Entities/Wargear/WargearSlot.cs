﻿using System.Collections.Generic;

namespace Kollection.Model.Entities
{
    public class WargearSlot
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual List<Wargear> Wargears { get; set; }
    }
}

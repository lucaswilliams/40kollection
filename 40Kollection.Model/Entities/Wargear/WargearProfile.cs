﻿namespace Kollection.Model.Entities
{
    public class WargearProfile
    {
        public int Id { get; set; }
        public int WargearId { get; set; }
        public virtual Wargear Wargear { get; set; }
        public string Range { get; set; }
        public string Type { get; set; }
        public string Strength { get; set; }
        public string AP { get; set; }
        public string Damage { get; set; }
        public string Abilities { get; set; }
    }
}

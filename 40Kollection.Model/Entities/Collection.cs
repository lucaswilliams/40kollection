﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Kollection.Model.Entities
{
    public class Collection
    {
        public int Id { get; set; }
        
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        [DisplayFormat(NullDisplayText = "http://placehold.it/300x300")]
        public string Image { get; set; }

        public virtual List<CollectionUnit> Units { get; set; }
    }
}

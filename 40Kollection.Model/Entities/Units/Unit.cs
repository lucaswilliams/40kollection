﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Kollection.Model.Entities
{
    public class Unit
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int UnitTypeId { get; set; }
        [Display(Name="Type")]
        public virtual UnitType UnitType { get; set; }

        public virtual List<UnitWargear> UnitWargear { get; set; }
        
        /// <summary>
        /// The base number of points that this unit is worth.  A Chaos Cultist is 4 points, where a Space Marine is 18 and a Daemon Prince is 145
        /// </summary>
        public int Points { get; set; }

        /// <summary>
        /// The image for the unit.  This is generic (probably from the codex) and can be overwritten on a "per army" basis.
        /// </summary>
        [DisplayFormat(NullDisplayText = "http://placehold.it/300x300")]
        public string Image { get; set; }

        /// <summary>
        /// This shows how accurate a warrior is with ranged weapons of all kinds, from pistols firing blazing bolts of plasma to earth-shaking battle cannons.
        /// The higher this characteristic is, the easier a creature finds it to hit targets with shooting attacks. An Imperial Guardsmanhas Ballistic Skill 3, but a hardened Militarum Tempestus Scion has Ballistic Skill 4.
        /// </summary>
        [Display(Name="BS")]
        public string Skill_Ballistic { get; set; }
        /// <summary>
        /// This characteristic defines the close combat skill a warrior possesses.
        /// The higher the characteristic, the more likely the model is to hit an opponent in close combat. An Imperial Guardsman (a trained human warrior) has Weapon Skill 3, whilst a superhuman Space Marine might have Weapon Skill 4, Weapon Skill 5 or possibly even higher!
        /// </summary>
        [Display(Name="WS")]
        public string Skill_Weapon { get; set; }
        /// <summary>
        /// Strength gives a measure of how physically mighty a warrior is. 
        /// An exceptionally puny creature might have Strength 1, while a Tyranid Carnifex has Strength 9. Humans have Strength 3.
        /// </summary>        
        [Display(Name="S")]
        public int? Strength { get; set; }
        /// <summary>
        /// This is a measure of a model’s ability to resist physical damage and pain, and it reflects such factors as the resilience of a creature’s flesh, hide or skin. 
        /// The tougher a model is, the better it can withstand an enemy’s blows. The gnarled and leathery hide of an Ork grants it Toughness 4, but an unyielding monster such as a Carnifex has an incredible Toughness of 6!
        /// </summary>
        [Display(Name = "T")]
        public int? Toughness { get; set; }
        /// <summary>
        /// The number of wounds that the model has.  Also stores the HP value for vehicles.
        /// </summary>
        [Display(Name = "W")]
        public int Wounds { get; set; }
        /// <summary>
        /// This represents the range a model can move. 
        /// Some flying models have a minimum movement
        /// </summary>
        [Display(Name = "M")]
        public int? Movement { get; set; }
        /// <summary>
        /// This shows the number of times a model attacks during close combat
        /// </summary>
        [Display(Name = "A")]
        public string Attacks { get; set; }
        /// <summary>
        /// Leadership reveals how courageous, determined and self-controlled a model is. 
        /// The higher the value, the more reliable the model under pressure. A creature with a low Leadership value is very unruly or cowardly, to say the least! Elite forces, such as Space Marines, have Leadership 8 or higher, whilst cowardly troops, such as Gretchin, have Leadership 5 or less.
        /// </summary>
        [Display(Name = "Ld")]
        public int? Leadership { get; set; }
        /// <summary>
        /// A warrior’s Armour Save gives it a chance to avoid harm when it is struck or shot. 
        /// Most models have an Armour Save based on what kind of armour they are wearing, so this characteristic may be improved if they are equipped with better armour. Other creatures may receive a natural save from having thick bony plates or a chitinous shell. Unlike other characteristics, the lower an Armour Save is, the better. A model can never have an Armour Save better than 2+.
        /// </summary>
        [Display(Name = "Sv")]
        public string ArmourSave { get; set; }
    }
}

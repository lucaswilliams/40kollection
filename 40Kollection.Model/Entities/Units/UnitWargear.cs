﻿namespace Kollection.Model.Entities
{
    /// <summary>
    /// A list of eligible upgrades for a unit, including a points modifier
    /// </summary>
    public class UnitWargear
    {
        public int Id { get; set; }

        /// <summary>
        /// The Unit that this upgrade applies to
        /// </summary>
        public int UnitId { get; set; }
        public virtual Unit Unit { get; set; }

        /// <summary>
        /// The upgrade we are applying to the unit
        /// </summary>
        public int WargearId { get; set; }
        public Wargear Wargear { get; set; }
    }
}

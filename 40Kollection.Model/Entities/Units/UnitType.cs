﻿namespace Kollection.Model.Entities
{
    public class UnitType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

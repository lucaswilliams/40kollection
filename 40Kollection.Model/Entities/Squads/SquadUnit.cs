﻿namespace Kollection.Model.Entities
{
    public class SquadUnit
    {
        public int Id { get; set; }
        
        public int SquadId { get; set; }
        public virtual Squad Squad { get; set; }

        public int CollectionUnitId { get; set; }
        public virtual CollectionUnit UnitConfiguration { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Kollection.Model.Entities
{
    public class Squad
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        [DisplayFormat(NullDisplayText = "http://placehold.it/300x300")]
        public string Image { get; set; }
        public int Points { get; set; }
        [Display(Name = "Squad Role")]
        public int SquadRoleId { get; set; }
        public virtual SquadRole SquadRole { get; set; }

        //Linked to a user
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        [Display(Name = "Part of Collection")]
        public int CollectionId { get; set; }
        public virtual Collection Collection { get; set; }

        public virtual List<SquadUnit> Units { get; set; }
    }
}

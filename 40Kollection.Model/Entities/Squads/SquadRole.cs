﻿namespace Kollection.Model.Entities
{
    public class SquadRole
    {
        public int Id { get; set; }
        /// <summary>
        /// The name of the role.
        /// Examples are HQ, Troop, Elite, Dedicated Transport
        /// </summary>
        public string Name { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Kollection.Model.Entities
{
    public class CollectionUnit
    {
        public int Id { get; set; }

        [Display(Name = "Unit")]
        public int UnitId { get; set; }
        public virtual Unit Unit { get; set; }

        [Display(Name = "Collection")]
        public int CollectionId { get; set; }
        public virtual Collection Collection { get; set; }

        public virtual List<CollectionUnitWargear> Wargears  { get; set; }
    }

    public class CollectionUnitWargear
    {
        public int Id { get; set; }

        public int CollectionUnitId { get; set; }

        public int WargearId { get; set; }
        public virtual Wargear Wargear { get; set; }
    }
}

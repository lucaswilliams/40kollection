namespace _40Kollection.Model.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Wargear_Images : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Wargears", "Description", c => c.String());
            AddColumn("dbo.Wargears", "Image", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Wargears", "Image");
            DropColumn("dbo.Wargears", "Description");
        }
    }
}

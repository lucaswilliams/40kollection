namespace _40Kollection.Model.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Wargear_Points : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Wargears", "Points", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Wargears", "Points");
        }
    }
}

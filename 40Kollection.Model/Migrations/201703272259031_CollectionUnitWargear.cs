namespace _40Kollection.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CollectionUnitWargear : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UnitConfigurations", "UnitId", "dbo.Units");
            DropForeignKey("dbo.Wargears", "UnitConfiguration_Id", "dbo.UnitConfigurations");
            DropForeignKey("dbo.UnitConfigurations", "CollectionUnit_Id", "dbo.CollectionUnits");
            DropIndex("dbo.Wargears", new[] { "UnitConfiguration_Id" });
            DropIndex("dbo.UnitConfigurations", new[] { "UnitId" });
            DropIndex("dbo.UnitConfigurations", new[] { "CollectionUnit_Id" });
            CreateTable(
                "dbo.CollectionUnitWargears",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CollectionUnitId = c.Int(nullable: false),
                        WargearId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CollectionUnits", t => t.CollectionUnitId, cascadeDelete: true)
                .Index(t => t.CollectionUnitId);
            
            AddColumn("dbo.Wargears", "CollectionUnitWargear_Id", c => c.Int());
            CreateIndex("dbo.Wargears", "CollectionUnitWargear_Id");
            AddForeignKey("dbo.Wargears", "CollectionUnitWargear_Id", "dbo.CollectionUnitWargears", "Id");
            DropColumn("dbo.Wargears", "UnitConfiguration_Id");
            DropTable("dbo.UnitConfigurations");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.UnitConfigurations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        UnitId = c.Int(nullable: false),
                        CollectionUnit_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Wargears", "UnitConfiguration_Id", c => c.Int());
            DropForeignKey("dbo.CollectionUnitWargears", "CollectionUnitId", "dbo.CollectionUnits");
            DropForeignKey("dbo.Wargears", "CollectionUnitWargear_Id", "dbo.CollectionUnitWargears");
            DropIndex("dbo.CollectionUnitWargears", new[] { "CollectionUnitId" });
            DropIndex("dbo.Wargears", new[] { "CollectionUnitWargear_Id" });
            DropColumn("dbo.Wargears", "CollectionUnitWargear_Id");
            DropTable("dbo.CollectionUnitWargears");
            CreateIndex("dbo.UnitConfigurations", "CollectionUnit_Id");
            CreateIndex("dbo.UnitConfigurations", "UnitId");
            CreateIndex("dbo.Wargears", "UnitConfiguration_Id");
            AddForeignKey("dbo.UnitConfigurations", "CollectionUnit_Id", "dbo.CollectionUnits", "Id");
            AddForeignKey("dbo.Wargears", "UnitConfiguration_Id", "dbo.UnitConfigurations", "Id");
            AddForeignKey("dbo.UnitConfigurations", "UnitId", "dbo.Units", "Id", cascadeDelete: true);
        }
    }
}

namespace _40Kollection.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _8thed_stats : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Units", "Movement", c => c.Int());
            DropColumn("dbo.Units", "Armour_Front");
            DropColumn("dbo.Units", "Armour_Side");
            DropColumn("dbo.Units", "Armour_Rear");
            DropColumn("dbo.Units", "Initiative");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Units", "Initiative", c => c.Int());
            AddColumn("dbo.Units", "Armour_Rear", c => c.Int());
            AddColumn("dbo.Units", "Armour_Side", c => c.Int());
            AddColumn("dbo.Units", "Armour_Front", c => c.Int());
            DropColumn("dbo.Units", "Movement");
        }
    }
}

namespace _40Kollection.Model.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Optional_Unit_Fields : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Units", "Skill_Weapon", c => c.Int());
            AlterColumn("dbo.Units", "Strength", c => c.Int());
            AlterColumn("dbo.Units", "Toughness", c => c.Int());
            AlterColumn("dbo.Units", "Armour_Front", c => c.Int());
            AlterColumn("dbo.Units", "Armour_Side", c => c.Int());
            AlterColumn("dbo.Units", "Armour_Rear", c => c.Int());
            AlterColumn("dbo.Units", "Initiative", c => c.Int());
            AlterColumn("dbo.Units", "Attacks", c => c.Int());
            AlterColumn("dbo.Units", "Leadership", c => c.Int());
            AlterColumn("dbo.Units", "ArmourSave", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Units", "ArmourSave", c => c.Int(nullable: false));
            AlterColumn("dbo.Units", "Leadership", c => c.Int(nullable: false));
            AlterColumn("dbo.Units", "Attacks", c => c.Int(nullable: false));
            AlterColumn("dbo.Units", "Initiative", c => c.Int(nullable: false));
            AlterColumn("dbo.Units", "Armour_Rear", c => c.Int(nullable: false));
            AlterColumn("dbo.Units", "Armour_Side", c => c.Int(nullable: false));
            AlterColumn("dbo.Units", "Armour_Front", c => c.Int(nullable: false));
            AlterColumn("dbo.Units", "Toughness", c => c.Int(nullable: false));
            AlterColumn("dbo.Units", "Strength", c => c.Int(nullable: false));
            AlterColumn("dbo.Units", "Skill_Weapon", c => c.Int(nullable: false));
        }
    }
}

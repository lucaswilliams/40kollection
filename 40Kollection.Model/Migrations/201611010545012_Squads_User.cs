namespace _40Kollection.Model.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Squads_User : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Squads", "UserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Squads", "UserId");
            AddForeignKey("dbo.Squads", "UserId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Squads", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.Squads", new[] { "UserId" });
            DropColumn("dbo.Squads", "UserId");
        }
    }
}

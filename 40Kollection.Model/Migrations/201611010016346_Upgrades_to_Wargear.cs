namespace _40Kollection.Model.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Upgrades_to_Wargear : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.UpgradeSlots", newName: "WargearSlots");
            DropForeignKey("dbo.Upgrades", "UpgradeSlotId", "dbo.UpgradeSlots");
            DropForeignKey("dbo.Upgrades", "UnitConfiguration_Id", "dbo.UnitConfigurations");
            DropForeignKey("dbo.UnitUpgrades", "UnitId", "dbo.Units");
            DropForeignKey("dbo.UnitUpgrades", "UpgradeId", "dbo.UnitUpgrades");
            DropIndex("dbo.Upgrades", new[] { "UpgradeSlotId" });
            DropIndex("dbo.Upgrades", new[] { "UnitConfiguration_Id" });
            DropIndex("dbo.UnitUpgrades", new[] { "UnitId" });
            DropIndex("dbo.UnitUpgrades", new[] { "UpgradeId" });
            CreateTable(
                "dbo.Wargears",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        WargearSlotId = c.Int(nullable: false),
                        UnitConfiguration_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.WargearSlots", t => t.WargearSlotId, cascadeDelete: true)
                .ForeignKey("dbo.UnitConfigurations", t => t.UnitConfiguration_Id)
                .Index(t => t.WargearSlotId)
                .Index(t => t.UnitConfiguration_Id);
            
            CreateTable(
                "dbo.UnitWargears",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UnitId = c.Int(nullable: false),
                        WargearId = c.Int(nullable: false),
                        Points = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Units", t => t.UnitId, cascadeDelete: true)
                .ForeignKey("dbo.Wargears", t => t.WargearId, cascadeDelete: true)
                .Index(t => t.UnitId)
                .Index(t => t.WargearId);
            
            DropTable("dbo.Upgrades");
            DropTable("dbo.UnitUpgrades");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.UnitUpgrades",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UnitId = c.Int(nullable: false),
                        UpgradeId = c.Int(nullable: false),
                        Points = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Upgrades",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        UpgradeSlotId = c.Int(nullable: false),
                        UnitConfiguration_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.UnitWargears", "WargearId", "dbo.Wargears");
            DropForeignKey("dbo.UnitWargears", "UnitId", "dbo.Units");
            DropForeignKey("dbo.Wargears", "UnitConfiguration_Id", "dbo.UnitConfigurations");
            DropForeignKey("dbo.Wargears", "WargearSlotId", "dbo.WargearSlots");
            DropIndex("dbo.UnitWargears", new[] { "WargearId" });
            DropIndex("dbo.UnitWargears", new[] { "UnitId" });
            DropIndex("dbo.Wargears", new[] { "UnitConfiguration_Id" });
            DropIndex("dbo.Wargears", new[] { "WargearSlotId" });
            DropTable("dbo.UnitWargears");
            DropTable("dbo.Wargears");
            CreateIndex("dbo.UnitUpgrades", "UpgradeId");
            CreateIndex("dbo.UnitUpgrades", "UnitId");
            CreateIndex("dbo.Upgrades", "UnitConfiguration_Id");
            CreateIndex("dbo.Upgrades", "UpgradeSlotId");
            AddForeignKey("dbo.UnitUpgrades", "UpgradeId", "dbo.UnitUpgrades", "Id");
            AddForeignKey("dbo.UnitUpgrades", "UnitId", "dbo.Units", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Upgrades", "UnitConfiguration_Id", "dbo.UnitConfigurations", "Id");
            AddForeignKey("dbo.Upgrades", "UpgradeSlotId", "dbo.UpgradeSlots", "Id", cascadeDelete: true);
            RenameTable(name: "dbo.WargearSlots", newName: "UpgradeSlots");
        }
    }
}

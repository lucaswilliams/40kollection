namespace _40Kollection.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class WargearProfiles : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.WargearProfiles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        WargearId = c.Int(nullable: false),
                        Range = c.String(),
                        Type = c.String(),
                        Strength = c.String(),
                        AP = c.String(),
                        Damage = c.String(),
                        Abilities = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Wargears", t => t.WargearId, cascadeDelete: true)
                .Index(t => t.WargearId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WargearProfiles", "WargearId", "dbo.Wargears");
            DropIndex("dbo.WargearProfiles", new[] { "WargearId" });
            DropTable("dbo.WargearProfiles");
        }
    }
}

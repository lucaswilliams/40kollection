namespace _40Kollection.Model.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Basic_Units : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Units",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        UnitTypeId = c.Int(nullable: false),
                        Skill_Ballistic = c.Int(nullable: false),
                        Skill_Weapon = c.Int(nullable: false),
                        Strength = c.Int(nullable: false),
                        Toughness = c.Int(nullable: false),
                        Armour_Front = c.Int(nullable: false),
                        Armour_Side = c.Int(nullable: false),
                        Armour_Rear = c.Int(nullable: false),
                        Wounds = c.Int(nullable: false),
                        Initiative = c.Int(nullable: false),
                        Attacks = c.Int(nullable: false),
                        Leadership = c.Int(nullable: false),
                        ArmourSave = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UnitTypes", t => t.UnitTypeId, cascadeDelete: true)
                .Index(t => t.UnitTypeId);
            
            CreateTable(
                "dbo.UnitTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Units", "UnitTypeId", "dbo.UnitTypes");
            DropIndex("dbo.Units", new[] { "UnitTypeId" });
            DropTable("dbo.UnitTypes");
            DropTable("dbo.Units");
        }
    }
}

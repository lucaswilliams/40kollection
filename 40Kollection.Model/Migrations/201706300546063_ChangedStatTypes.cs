namespace _40Kollection.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedStatTypes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Wargears", "Strength", c => c.String());
            AddColumn("dbo.Wargears", "AP", c => c.String());
            AddColumn("dbo.Wargears", "Damage", c => c.String());
            AlterColumn("dbo.Units", "Skill_Ballistic", c => c.String());
            AlterColumn("dbo.Units", "Skill_Weapon", c => c.String());
            AlterColumn("dbo.Units", "Attacks", c => c.String());
            AlterColumn("dbo.Units", "ArmourSave", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Units", "ArmourSave", c => c.Int());
            AlterColumn("dbo.Units", "Attacks", c => c.Int());
            AlterColumn("dbo.Units", "Skill_Weapon", c => c.Int());
            AlterColumn("dbo.Units", "Skill_Ballistic", c => c.Int(nullable: false));
            DropColumn("dbo.Wargears", "Damage");
            DropColumn("dbo.Wargears", "AP");
            DropColumn("dbo.Wargears", "Strength");
        }
    }
}

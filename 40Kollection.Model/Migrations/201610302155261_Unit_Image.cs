namespace _40Kollection.Model.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Unit_Image : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Units", "Image", c => c.Binary());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Units", "Image");
        }
    }
}

namespace _40Kollection.Model.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Unit_Role_Addition : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UnitRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Units", "UnitRoleId", c => c.Int(nullable: false));
            CreateIndex("dbo.Units", "UnitRoleId");
            AddForeignKey("dbo.Units", "UnitRoleId", "dbo.UnitRoles", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Units", "UnitRoleId", "dbo.UnitRoles");
            DropIndex("dbo.Units", new[] { "UnitRoleId" });
            DropColumn("dbo.Units", "UnitRoleId");
            DropTable("dbo.UnitRoles");
        }
    }
}

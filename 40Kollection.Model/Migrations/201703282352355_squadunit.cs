namespace _40Kollection.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class squadunit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SquadUnits",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SquadId = c.Int(nullable: false),
                        CollectionUnitId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Squads", t => t.SquadId, cascadeDelete: false)
                .ForeignKey("dbo.CollectionUnits", t => t.CollectionUnitId, cascadeDelete: false)
                .Index(t => t.SquadId)
                .Index(t => t.CollectionUnitId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SquadUnits", "CollectionUnitId", "dbo.CollectionUnits");
            DropForeignKey("dbo.SquadUnits", "SquadId", "dbo.Squads");
            DropIndex("dbo.SquadUnits", new[] { "CollectionUnitId" });
            DropIndex("dbo.SquadUnits", new[] { "SquadId" });
            DropTable("dbo.SquadUnits");
        }
    }
}

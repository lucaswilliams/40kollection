namespace _40Kollection.Model.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Unit_Upgrades : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UnitConfigurations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        UnitId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Units", t => t.UnitId, cascadeDelete: true)
                .Index(t => t.UnitId);
            
            CreateTable(
                "dbo.Upgrades",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        UpgradeSlotId = c.Int(nullable: false),
                        Unit_Id = c.Int(),
                        UnitConfiguration_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UpgradeSlots", t => t.UpgradeSlotId, cascadeDelete: true)
                .ForeignKey("dbo.Units", t => t.Unit_Id)
                .ForeignKey("dbo.UnitConfigurations", t => t.UnitConfiguration_Id)
                .Index(t => t.UpgradeSlotId)
                .Index(t => t.Unit_Id)
                .Index(t => t.UnitConfiguration_Id);
            
            CreateTable(
                "dbo.UpgradeSlots",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Upgrades", "UnitConfiguration_Id", "dbo.UnitConfigurations");
            DropForeignKey("dbo.UnitConfigurations", "UnitId", "dbo.Units");
            DropForeignKey("dbo.Upgrades", "Unit_Id", "dbo.Units");
            DropForeignKey("dbo.Upgrades", "UpgradeSlotId", "dbo.UpgradeSlots");
            DropIndex("dbo.Upgrades", new[] { "UnitConfiguration_Id" });
            DropIndex("dbo.Upgrades", new[] { "Unit_Id" });
            DropIndex("dbo.Upgrades", new[] { "UpgradeSlotId" });
            DropIndex("dbo.UnitConfigurations", new[] { "UnitId" });
            DropTable("dbo.UpgradeSlots");
            DropTable("dbo.Upgrades");
            DropTable("dbo.UnitConfigurations");
        }
    }
}

namespace _40Kollection.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class WargearPoints : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.UnitWargears", "Points");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UnitWargears", "Points", c => c.Int(nullable: false));
        }
    }
}

namespace _40Kollection.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Squads_And_Configs : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CollectionUnits",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UnitId = c.Int(nullable: false),
                        CollectionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Collections", t => t.CollectionId, cascadeDelete: true)
                .ForeignKey("dbo.Units", t => t.UnitId, cascadeDelete: true)
                .Index(t => t.UnitId)
                .Index(t => t.CollectionId);
            
            AddColumn("dbo.Squads", "CollectionId", c => c.Int(nullable: false));
            AddColumn("dbo.UnitConfigurations", "CollectionUnit_Id", c => c.Int());
            CreateIndex("dbo.UnitConfigurations", "CollectionUnit_Id");
            CreateIndex("dbo.Squads", "CollectionId");
            AddForeignKey("dbo.UnitConfigurations", "CollectionUnit_Id", "dbo.CollectionUnits", "Id");
            AddForeignKey("dbo.Squads", "CollectionId", "dbo.Collections", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Squads", "CollectionId", "dbo.Collections");
            DropForeignKey("dbo.UnitConfigurations", "CollectionUnit_Id", "dbo.CollectionUnits");
            DropForeignKey("dbo.CollectionUnits", "UnitId", "dbo.Units");
            DropForeignKey("dbo.CollectionUnits", "CollectionId", "dbo.Collections");
            DropIndex("dbo.Squads", new[] { "CollectionId" });
            DropIndex("dbo.UnitConfigurations", new[] { "CollectionUnit_Id" });
            DropIndex("dbo.CollectionUnits", new[] { "CollectionId" });
            DropIndex("dbo.CollectionUnits", new[] { "UnitId" });
            DropColumn("dbo.UnitConfigurations", "CollectionUnit_Id");
            DropColumn("dbo.Squads", "CollectionId");
            DropTable("dbo.CollectionUnits");
        }
    }
}

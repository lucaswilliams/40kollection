namespace _40Kollection.Model.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Unit_Upgrades_part_2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Upgrades", "Unit_Id", "dbo.Units");
            DropIndex("dbo.Upgrades", new[] { "Unit_Id" });
            CreateTable(
                "dbo.UnitUpgrades",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UnitId = c.Int(nullable: false),
                        UpgradeId = c.Int(nullable: false),
                        Points = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Units", t => t.UnitId, cascadeDelete: true)
                .ForeignKey("dbo.UnitUpgrades", t => t.UpgradeId)
                .Index(t => t.UnitId)
                .Index(t => t.UpgradeId);
            
            AddColumn("dbo.Units", "Points", c => c.Int(nullable: false));
            DropColumn("dbo.Upgrades", "Unit_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Upgrades", "Unit_Id", c => c.Int());
            DropForeignKey("dbo.UnitUpgrades", "UpgradeId", "dbo.UnitUpgrades");
            DropForeignKey("dbo.UnitUpgrades", "UnitId", "dbo.Units");
            DropIndex("dbo.UnitUpgrades", new[] { "UpgradeId" });
            DropIndex("dbo.UnitUpgrades", new[] { "UnitId" });
            DropColumn("dbo.Units", "Points");
            DropTable("dbo.UnitUpgrades");
            CreateIndex("dbo.Upgrades", "Unit_Id");
            AddForeignKey("dbo.Upgrades", "Unit_Id", "dbo.Units", "Id");
        }
    }
}

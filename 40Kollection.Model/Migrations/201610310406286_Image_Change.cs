namespace Kollection.Model.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Image_Change : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Units", "Image", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Units", "Image", c => c.Binary());
        }
    }
}

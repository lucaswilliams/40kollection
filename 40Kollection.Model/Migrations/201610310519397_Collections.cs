namespace _40Kollection.Model.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Collections : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Collections",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        Name = c.String(),
                        Description = c.String(),
                        Image = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Collections", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.Collections", new[] { "UserId" });
            DropTable("dbo.Collections");
        }
    }
}

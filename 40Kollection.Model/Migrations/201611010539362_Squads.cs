namespace _40Kollection.Model.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Squads : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.UnitRoles", newName: "SquadRoles");
            DropForeignKey("dbo.Units", "UnitRoleId", "dbo.UnitRoles");
            DropIndex("dbo.Units", new[] { "UnitRoleId" });
            CreateTable(
                "dbo.Squads",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Image = c.String(),
                        Points = c.Int(nullable: false),
                        SquadRoleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SquadRoles", t => t.SquadRoleId, cascadeDelete: true)
                .Index(t => t.SquadRoleId);
            
            DropColumn("dbo.Units", "UnitRoleId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Units", "UnitRoleId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Squads", "SquadRoleId", "dbo.SquadRoles");
            DropIndex("dbo.Squads", new[] { "SquadRoleId" });
            DropTable("dbo.Squads");
            CreateIndex("dbo.Units", "UnitRoleId");
            AddForeignKey("dbo.Units", "UnitRoleId", "dbo.UnitRoles", "Id", cascadeDelete: true);
            RenameTable(name: "dbo.SquadRoles", newName: "UnitRoles");
        }
    }
}

namespace _40Kollection.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class anything : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Wargears", "CollectionUnitWargear_Id", "dbo.CollectionUnitWargears");
            DropIndex("dbo.Wargears", new[] { "CollectionUnitWargear_Id" });
            CreateIndex("dbo.CollectionUnitWargears", "WargearId");
            AddForeignKey("dbo.CollectionUnitWargears", "WargearId", "dbo.Wargears", "Id", cascadeDelete: true);
            DropColumn("dbo.Wargears", "CollectionUnitWargear_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Wargears", "CollectionUnitWargear_Id", c => c.Int());
            DropForeignKey("dbo.CollectionUnitWargears", "WargearId", "dbo.Wargears");
            DropIndex("dbo.CollectionUnitWargears", new[] { "WargearId" });
            CreateIndex("dbo.Wargears", "CollectionUnitWargear_Id");
            AddForeignKey("dbo.Wargears", "CollectionUnitWargear_Id", "dbo.CollectionUnitWargears", "Id");
        }
    }
}

namespace _40Kollection.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MoreWeaponStats : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Wargears", "Range", c => c.String());
            AddColumn("dbo.Wargears", "Type", c => c.String());
            AddColumn("dbo.Wargears", "Abilities", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Wargears", "Abilities");
            DropColumn("dbo.Wargears", "Type");
            DropColumn("dbo.Wargears", "Range");
        }
    }
}

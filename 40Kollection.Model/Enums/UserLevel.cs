﻿namespace Kollection.Model.Enums
{
    public enum UserLevel
    {
        NotLoggedIn,
        Freebie,
        Subscriber,
        Administrator
    }
}

using Kollection.Model.Entities;
using System;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Kollection.Model
{
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class KollectionEntities : IdentityDbContext<ApplicationUser>
    {
        // Your context has been configured to use a 'KollectionEntities' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'Kollection.Model.KollectionEntities' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'KollectionEntities' 
        // connection string in the application configuration file.
        public KollectionEntities()
            : base("name=KollectionEntities", throwIfV1Schema: false)
        {
        }

        public static KollectionEntities Create()
        {
            return new KollectionEntities();
        }

        public override int SaveChanges()
        {
            try
            {
                //calls the base save changes
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ve)
            {
                //This sorts out the validation errors.
                var validationErrors = string.Join("; ",
                    ve.EntityValidationErrors.SelectMany(result => result.ValidationErrors)
                        .Select(error => error.ErrorMessage));

                throw new Exception("Entity Framework validation errors occurred while trying to save record: " + validationErrors, ve);
            }
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.
        public virtual DbSet<Collection> Collections { get; set; }
        public virtual DbSet<CollectionUnit> CollectionUnits { get; set; }
        public virtual DbSet<CollectionUnitWargear> CollectionUnitWargears { get; set; }

        public virtual DbSet<Unit> Units { get; set; }
        public virtual DbSet<UnitType> UnitTypes { get; set; }
        public virtual DbSet<UnitWargear> UnitWargears { get; set; }
        
        public virtual DbSet<WargearSlot> WargearSlots { get; set; }
        public virtual DbSet<Wargear> Wargears { get; set; }

        public virtual DbSet<Squad> Squads { get; set; }
        public virtual DbSet<SquadRole> SquadRoles { get; set; }
        public virtual DbSet<SquadUnit> SquadUnits { get; set; }
    }
}
﻿$('.fileupload').fileupload({
    url: '/Image/AjaxSubmit',
    done: function (e, data) {
        $('.upload-preview').attr('src', data.result);
        $('.image-src').val(data.result);
    }
});
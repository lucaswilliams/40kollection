﻿using Kollection.Model;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Kollection.Startup))]
namespace Kollection
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            ConfigureRoles();
        }

        private void ConfigureRoles()
        {
            KollectionEntities context = new KollectionEntities();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            userManager.UserValidator = new UserValidator<ApplicationUser>(userManager)
            {
                AllowOnlyAlphanumericUserNames = false
            };

            if (!roleManager.RoleExists("Admin"))
            {
                var role = new IdentityRole();
                role.Name = "Admin";
                roleManager.Create(role);                
            }

            var tempUser = userManager.FindByEmail("lucas@lucas-williams.com");
            if (tempUser == null)
            {
                var user = new ApplicationUser();
                user.Email = "lucas@lucas-williams.com";
                user.UserName = "lucas@lucas-williams.com";
                string password = "@tlant1S";
                var chkUser = userManager.Create(user, password);

                if (chkUser.Succeeded)
                {
                    var result1 = userManager.AddToRole(user.Id, "Admin");
                } else
                {
                    throw new System.Exception("There was an error setting up the system user: " + string.Join(", ", chkUser.Errors));
                }
            }

            if (!roleManager.RoleExists("Subscriber"))
            {
                var role = new IdentityRole();
                role.Name = "Subscriber";
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists("Freebie"))
            {
                var role = new IdentityRole();
                role.Name = "Freebie";
                roleManager.Create(role);
            }
        }
    }
}

﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Kollection.Model;
using Kollection.Model.Entities;

namespace Kollection.Controllers.Squads
{
    public class SquadRolesController : Controller
    {
        private KollectionEntities db = new KollectionEntities();

        // GET: SquadRoles
        public ActionResult Index()
        {
            return View(db.SquadRoles.OrderBy(t => t.Name).ToList());
        }

        // GET: SquadRoles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SquadRole unitRole = db.SquadRoles.Find(id);
            if (unitRole == null)
            {
                return HttpNotFound();
            }
            return View(unitRole);
        }

        // GET: SquadRoles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SquadRoles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name")] SquadRole unitRole)
        {
            if (ModelState.IsValid)
            {
                db.SquadRoles.Add(unitRole);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(unitRole);
        }

        // GET: SquadRoles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SquadRole unitRole = db.SquadRoles.Find(id);
            if (unitRole == null)
            {
                return HttpNotFound();
            }
            return View(unitRole);
        }

        // POST: SquadRoles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name")] SquadRole unitRole)
        {
            if (ModelState.IsValid)
            {
                db.Entry(unitRole).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(unitRole);
        }

        // GET: SquadRoles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SquadRole unitRole = db.SquadRoles.Find(id);
            if (unitRole == null)
            {
                return HttpNotFound();
            }
            return View(unitRole);
        }

        // POST: SquadRoles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SquadRole unitRole = db.SquadRoles.Find(id);
            db.SquadRoles.Remove(unitRole);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Kollection.Model;
using Kollection.Model.Entities;
using Microsoft.AspNet.Identity;
using Kollection.ViewModels;

namespace Kollection.Controllers.Squads
{
    [OutputCache(NoStore = true, Duration = 0)]
    public class SquadsController : Controller
    {
        private KollectionEntities db = new KollectionEntities();

        // GET: Squads
        public ActionResult Index()
        {
            string userId = User.Identity.GetUserId();
            var squads = db.Squads.Include(s => s.SquadRole).Include(c => c.User).Where(c => c.UserId == userId);
            
            return View(squads.OrderBy(s => s.Name).ToList());
        }

        // GET: Squads/ShowAll
        public ActionResult ShowAll()
        {
            if (User.IsInRole("Admin"))
            {
                var squads = db.Squads.Include(s => s.SquadRole).Include(c => c.User);
                return View(squads.OrderBy(s => s.Name).ToList());
            }
            
            return new HttpStatusCodeResult(403);
        }

        // GET: Squads/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Squad squad = db.Squads.Find(id);
            if (squad == null)
            {
                return HttpNotFound();
            }
            return View(squad);
        }

        // GET: Squads/Create
        public ActionResult Create()
        {
            string userId = User.Identity.GetUserId();
            ViewBag.SquadRoleId = new SelectList(db.SquadRoles.OrderBy(r => r.Name), "Id", "Name");
            ViewBag.CollectionId = new SelectList(db.Collections.Where(c => c.UserId == userId).OrderBy(c => c.Name), "Id", "Name");
            ViewBag.Units = db.Units.OrderBy(u => u.Name);
            return View();
        }

        // POST: Squads/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name,UserId,Description,Image,SquadRoleId,CollectionId")] Squad squad)
        {
            if (ModelState.IsValid)
            {
                db.Squads.Add(squad);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            string userId = User.Identity.GetUserId();
            ViewBag.SquadRoleId = new SelectList(db.SquadRoles.OrderBy(r => r.Name), "Id", "Name");
            ViewBag.CollectionId = new SelectList(db.Collections.Where(c => c.UserId == userId).OrderBy(c => c.Name), "Id", "Name");
            ViewBag.Units = db.Units.OrderBy(u => u.Name);
            return View(squad);
        }

        // GET: Squads/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Squad squad = db.Squads.Find(id);
            if (squad == null)
            {
                return HttpNotFound();
            }

            string userId = User.Identity.GetUserId();
            ViewBag.SquadRoleId = new SelectList(db.SquadRoles.OrderBy(r => r.Name), "Id", "Name", squad.SquadRoleId);
            ViewBag.CollectionId = new SelectList(db.Collections.Where(c => c.UserId == userId).OrderBy(c => c.Name), "Id", "Name", squad.CollectionId);
            ViewBag.Units = db.Units.OrderBy(u => u.Name);

            SquadViewModel vm = new SquadViewModel()
            {
                Name = squad.Name,
                CollectionId = squad.CollectionId,
                Description = squad.Description,
                Id = squad.Id,
                Image = squad.Image,
                Points = squad.Points,
                SquadRoleId = squad.SquadRoleId,
                UserId = squad.UserId
            };

            var units = db.Database.SqlQuery<UnitWargearViewModel>(
                "select " +
                "un.Name as UnitName, cu.Id as CollectionUnitId, ( " +
                "    select distinct wg.Name as li " +
                "" +
                "    from Wargears wg " +
                "" +
                "    join CollectionUnitWargears cuw on cuw.WargearId = wg.Id " +
                "" +
                "    where cuw.CollectionUnitId = cu.Id " +
                "" +
                "    for xml path('') " +
                ") as UnitWargear " +
                "from " +
                "Units un " +
                "join CollectionUnits cu on cu.UnitId = un.Id " +
                "join SquadUnits su on su.CollectionUnitId = cu.Id " +
                "where " +
                $"su.SquadId = {id} "
            ).ToList();

            vm.Units = units;

            return View(vm);
        }

        // POST: Squads/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Description,Image,SquadRoleId,CollectionId,UserId")] Squad squad)
        {
            if (ModelState.IsValid)
            {
                db.Entry(squad).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            string userId = User.Identity.GetUserId();
            ViewBag.SquadRoleId = new SelectList(db.SquadRoles.OrderBy(r => r.Name), "Id", "Name", squad.SquadRoleId);
            ViewBag.CollectionId = new SelectList(db.Collections.Where(c => c.UserId == userId).OrderBy(c => c.Name), "Id", "Name", squad.CollectionId);
            ViewBag.Units = db.Units.OrderBy(u => u.Name);

            SquadViewModel vm = new SquadViewModel()
            {
                Name = squad.Name,
                CollectionId = squad.CollectionId,
                Description = squad.Description,
                Id = squad.Id,
                Image = squad.Image,
                Points = squad.Points,
                SquadRoleId = squad.SquadRoleId,
                UserId = squad.UserId
            };

            return View(vm);
        }

        [HttpPost]
        public ActionResult AddUnits(int id, int[] collectionUnits)
        {
            foreach(int unit in collectionUnits)
            {
                db.SquadUnits.Add(new SquadUnit()
                {
                    SquadId = id,
                    CollectionUnitId = unit
                });
            }
            db.SaveChanges();
            int _points = 0;

            var squadUnits = db.SquadUnits.Where(su => su.SquadId == id).ToList();
            foreach(var squadUnit in squadUnits)
            {
                _points += squadUnit.UnitConfiguration.Unit.Points;

                foreach(var wargear in squadUnit.UnitConfiguration.Wargears)
                {
                    _points += wargear.Wargear.Points;
                }
            }

            var result = new { success = true, points = _points };
            return Json(result);
        }
        
        public ActionResult RemoveUnit(int id)
        {
            db.SquadUnits.RemoveRange(db.SquadUnits.Where(s => s.CollectionUnitId == id));
            db.SaveChanges();
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        // GET: Squads/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Squad squad = db.Squads.Find(id);
            if (squad == null)
            {
                return HttpNotFound();
            }
            return View(squad);
        }

        // POST: Squads/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            db.SquadUnits.RemoveRange(db.SquadUnits.Where(s => s.SquadId == id).ToList());
            db.SaveChanges();

            Squad squad = db.Squads.Find(id);
            db.Squads.Remove(squad);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Kollection.Model;
using Kollection.Model.Entities;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using Kollection.ViewModels;
using MoreLinq;

namespace Kollection.Controllers
{
    [OutputCache(NoStore = true, Duration = 0)]
    public class CollectionsController : Controller
    {
        private KollectionEntities db = new KollectionEntities();

        // GET: Collections
        public ActionResult Index()
        {
            string userId = User.Identity.GetUserId();
            var collections = db.Collections.Include(c => c.User).Where(c => c.UserId == userId);
            
            ViewBag.MaxCollections = GetMaxCollections();
            return View(collections.ToList());
        }

        // GET: Collections/ShowAll
        public ActionResult ShowAll()
        {
            if (User.IsInRole("Admin"))
            {
                var collections = db.Collections.Include(c => c.User);

                ViewBag.MaxCollections = GetMaxCollections();
                return View(collections.ToList());
            }

            return new HttpStatusCodeResult(403);
        }

        // GET: Collections/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Collection collection = db.Collections.Find(id);
            if (collection == null)
            {
                return HttpNotFound();
            }

            CollectionDetailsViewModel vm = new CollectionDetailsViewModel()
            {
                Description = collection.Description,
                Id = collection.Id,
                Image = collection.Image,
                Name = collection.Name,
                UserId = collection.UserId,
                Units = new List<CollectionDetailsUnitViewModel>()
            };

            var units = db.Database.SqlQuery<CollectionDetailsUnitViewModel>(
                "select " +
                "un.Name, ( " +
                "    select distinct wg.Name as li " +
                "" +
                "    from Wargears wg " +
                "" +
                "    join CollectionUnitWargears cuw on cuw.WargearId = wg.Id " +
                "" +
                "    where cuw.CollectionUnitId = cu.Id " +
                "" +
                "    for xml path('') " +
                ") as Wargear " +
                "from " +
                "Units un " +
                "join CollectionUnits cu on cu.UnitId = un.Id " +
                "where " +
                $"cu.CollectionId = {id}"
            ).ToList();

            var distUnits = units.DistinctBy(x => x.Name+x.Wargear).ToList();
            
            foreach(var unit in distUnits)
            {
                //now get the quantity
                int count = units.Count(x => x.Name + x.Wargear == unit.Name + unit.Wargear);
                unit.Quantity = count;
                vm.Units.Add(unit);
            }

            return View(vm);
        }

        // GET: Collections/Create
        public ActionResult Create()
        {
            string userId = User.Identity.GetUserId();
            var collections = db.Collections.Include(c => c.User).Where(c => c.UserId == userId);
            if (collections.Count() < this.GetMaxCollections())
            {
                ViewBag.Units = db.Units.OrderBy(s => s.Name).ToList();
                return View();
            } 
            else
            {
                return RedirectToAction("Index");
            }
        }

        // POST: Collections/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UserId,Name,Description,Image")] Collection collection)
        {
            if (ModelState.IsValid)
            {
                db.Collections.Add(collection);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Units = db.Units.OrderBy(s => s.Name).ToList();
            return View(collection);
        }

        // GET: Collections/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Collection collection = db.Collections.Find(id);
            if (collection == null)
            {
                return HttpNotFound();
            }

            //We can assume this because the "find" from earlier was a success.
            collection = db.Collections.Include(c => c.Units).First(c => c.Id == id);

            //These are for the drop-down list
            ViewBag.Units = db.Units.OrderBy(s => s.Name).ToList();
            return View(collection);
        }

        // POST: Collections/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,UserId,Name,Description,Image")] Collection collection)
        {
            if (ModelState.IsValid)
            {
                db.Entry(collection).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //These are for the drop-down list
            ViewBag.Units = db.Units.OrderBy(s => s.Name).ToList();
            return View(collection);
        }

        // GET: Collections/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Collection collection = db.Collections.Find(id);
            if (collection == null)
            {
                return HttpNotFound();
            }
            return View(collection);
        }

        // POST: Collections/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Collection collection = db.Collections.Find(id);
            db.Collections.Remove(collection);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult AddUnits(int collectionId, int unitId, int[] wargearIds, int quantity)
        {
            for (int i = 0; i < quantity; i++)
            {
                CollectionUnit collUnit = new CollectionUnit() { CollectionId = collectionId, UnitId = unitId, Wargears = new List<CollectionUnitWargear>() };
                foreach (int wg in wargearIds) {
                    //Get the wargear itself
                    Wargear gear = db.Wargears.FirstOrDefault(g => g.Id == wg);
                    collUnit.Wargears.Add(new CollectionUnitWargear() { Wargear = gear });
                }
                db.CollectionUnits.Add(collUnit);
                db.SaveChanges();
            }

            var result = new { success = true };
            return Json(result);
        }

        private int GetMaxCollections()
        {
            if (User.IsInRole("Admin"))
            {
                return 999;
            }
            else
            {
                if (User.IsInRole("Subscriber"))
                {
                    return 10;
                }
                else
                {
                    return 1;
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

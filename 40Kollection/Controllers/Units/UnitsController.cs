﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Kollection.Model;
using Kollection.Model.Entities;
using Kollection.ViewModels;
using Microsoft.AspNet.Identity;

namespace Kollection.Controllers.Units
{
    [OutputCache(NoStore = true, Duration = 0)]
    public class UnitsController : Controller
    {
        private KollectionEntities db = new KollectionEntities();

        // GET: Units
        public ActionResult Index()
        {
            var units = db.Units.Include(u => u.UnitType).OrderBy(u => u.Name);
            return View(units.ToList());
        }

        // GET: Units/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Unit unit = db.Units.Find(id);
            if (unit == null)
            {
                return HttpNotFound();
            }
            return View(unit);
        }

        // GET: Units/Create
        public ActionResult Create()
        {
            ViewBag.UnitTypeId = new SelectList(db.UnitTypes.OrderBy(t => t.Name), "Id", "Name");
            ViewBag.WargearSlots = db.WargearSlots.Include(w => w.Wargears).OrderBy(s => s.Name).ToList();
            ViewBag.UnitWargear = db.UnitWargears.Where(u => u.WargearId == null).ToList();
            return View();
        }

        // POST: Units/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name,UnitTypeId,Skill_Ballistic,Skill_Weapon,Strength,Toughness,Movement,Wounds,Attacks,Leadership,ArmourSave,Image,Points")] Unit unit, int[] wargearid, int[] wargearpoints)
        {
            if (ModelState.IsValid)
            {
                //TODO: Here we could enforce rules about which fields are needed for which model type (e.g. Infantry need wounds, Tanks need armour)
                db.Units.Add(unit);

                if (wargearid != null)
                {
                    for (int i = 0; i < wargearid.Length; i++)
                    {
                        db.UnitWargears.Add(new UnitWargear()
                        {
                            UnitId = unit.Id,
                            WargearId = wargearid[i],
                        });
                    }
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UnitTypeId = new SelectList(db.UnitTypes.OrderBy(t => t.Name), "Id", "Name", unit.UnitTypeId);
            ViewBag.WargearSlots = db.WargearSlots.Include(w => w.Wargears).OrderBy(s => s.Name).ToList();
            ViewBag.UnitWargear = db.UnitWargears.Where(u => u.WargearId == null).ToList();
            return View(unit);
        }

        // GET: Units/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Unit unit = db.Units.Find(id);
            if (unit == null)
            {
                return HttpNotFound();
            }
            ViewBag.UnitTypeId = new SelectList(db.UnitTypes.OrderBy(t => t.Name), "Id", "Name", unit.UnitTypeId);
            ViewBag.WargearSlots = db.WargearSlots.Include(w => w.Wargears).OrderBy(s => s.Name).ToList();
            ViewBag.UnitWargear = db.UnitWargears.Where(u => u.UnitId == unit.Id).ToList();
            return View(unit);
        }

        // POST: Units/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,UnitTypeId,Skill_Ballistic,Skill_Weapon,Strength,Toughness,Movement,Wounds,Attacks,Leadership,ArmourSave,Image,Points")] Unit unit, int[] wargearid, int[] wargearpoints)
        {
            if (ModelState.IsValid)
            {
                db.Entry(unit).State = EntityState.Modified;
                //death to the false emperor, and existing wargear
                db.UnitWargears.RemoveRange(db.UnitWargears.Where(u => u.UnitId == unit.Id));

                if (wargearid != null)
                {
                    for (int i = 0; i < wargearid.Length; i++)
                    {
                        db.UnitWargears.Add(new UnitWargear()
                        {
                            UnitId = unit.Id,
                            WargearId = wargearid[i],
                        });
                    }
                }

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UnitTypeId = new SelectList(db.UnitTypes.OrderBy(t => t.Name), "Id", "Name", unit.UnitTypeId);
            return View(unit);
        }

        // GET: Units/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Unit unit = db.Units.Find(id);
            if (unit == null)
            {
                return HttpNotFound();
            }
            return View(unit);
        }

        // POST: Units/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Unit unit = db.Units.Find(id);
            db.Units.Remove(unit);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Modal/{id}
        public ActionResult Modal(int id)
        {
            Unit unit = db.Units.FirstOrDefault(u => u.Id == id);
            List<UnitModalWargearSlotViewModel> gearList = new List<UnitModalWargearSlotViewModel>();
            if (unit == null)
            {
                return new HttpNotFoundResult();
            }

            List<WargearSlot> slots = db.Wargears.Where(wg => wg.UnitWargears.Select(uw => uw.UnitId).Contains(unit.Id)).Select(wg => wg.WargearSlot).Distinct().ToList();
            List<UnitWargear> unitWargear = db.UnitWargears.Where(u => u.UnitId == id).Include(u => u.Wargear).ToList();

            foreach (WargearSlot slot in slots)
            {
                gearList.Add(new UnitModalWargearSlotViewModel()
                {
                    Name = slot.Name,
                    Wargears = unitWargear.Where(u => u.Wargear.WargearSlotId == slot.Id).Select(w => w.Wargear).ToList()
                });
            }
            
            UnitModalViewModel vm = new UnitModalViewModel()
            {
                Unit = unit,
                WargearSlots = gearList
            };

            return View(vm);
        }

        public ActionResult ModalCollection(int id, int collectionId)
        {
            Unit unit = db.Units.FirstOrDefault(u => u.Id == id);
            
            var units = db.Database.SqlQuery<UnitWargearViewModel>(
                "select " +
                "un.Name as UnitName, cu.Id as CollectionUnitId, ( " +
                "    select distinct wg.Name as li " +
                "" +
                "    from Wargears wg " +
                "" +
                "    join CollectionUnitWargears cuw on cuw.WargearId = wg.Id " +
                "" +
                "    where cuw.CollectionUnitId = cu.Id " +
                "" +
                "    for xml path('') " +
                ") as UnitWargear " +
                "from " +
                "Units un " +
                "join CollectionUnits cu on cu.UnitId = un.Id " +
                "join Collections co on co.Id = cu.CollectionId " +
                "where " +
                "not exists (select * from SquadUnits where SquadUnits.CollectionUnitId = cu.Id) " +
                $"and un.Id = {id} " +
                $"and co.Id = '{collectionId}'"
            ).ToList();

            AddUnitToSquadViewModel vm = new AddUnitToSquadViewModel()
            {
                UnitName = unit.Name,
                UnitGear = units
            };
            return View(vm);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

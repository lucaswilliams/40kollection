﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Kollection.Model;
using Kollection.Model.Entities;

namespace Kollection.Controllers.Wargears
{
    public class WargearSlotsController : Controller
    {
        private KollectionEntities db = new KollectionEntities();

        // GET: WargearSlots
        public ActionResult Index()
        {
            return View(db.WargearSlots.OrderBy(t => t.Name).ToList());
        }

        // GET: WargearSlots/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WargearSlot wargearSlot = db.WargearSlots.Find(id);
            if (wargearSlot == null)
            {
                return HttpNotFound();
            }
            return View(wargearSlot);
        }

        // GET: WargearSlots/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: WargearSlots/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name")] WargearSlot wargearSlot)
        {
            if (ModelState.IsValid)
            {
                db.WargearSlots.Add(wargearSlot);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(wargearSlot);
        }

        // GET: WargearSlots/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WargearSlot wargearSlot = db.WargearSlots.Find(id);
            if (wargearSlot == null)
            {
                return HttpNotFound();
            }
            return View(wargearSlot);
        }

        // POST: WargearSlots/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name")] WargearSlot wargearSlot)
        {
            if (ModelState.IsValid)
            {
                db.Entry(wargearSlot).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(wargearSlot);
        }

        // GET: WargearSlots/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WargearSlot WargearSlot = db.WargearSlots.Find(id);
            if (WargearSlot == null)
            {
                return HttpNotFound();
            }
            return View(WargearSlot);
        }

        // POST: WargearSlots/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            WargearSlot WargearSlot = db.WargearSlots.Find(id);
            db.WargearSlots.Remove(WargearSlot);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Kollection.Model;
using Kollection.Model.Entities;

namespace Kollection.Controllers.Wargears
{
    public class WargearsController : Controller
    {
        private KollectionEntities db = new KollectionEntities();

        // GET: Wargears
        public ActionResult Index()
        {
            var Wargears = db.Wargears.Include(u => u.WargearSlot).OrderBy(t => t.Name);
            return View(Wargears.ToList());
        }

        // GET: Wargears/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Wargear wargear = db.Wargears.Find(id);
            if (wargear == null)
            {
                return HttpNotFound();
            }
            return View(wargear);
        }

        // GET: Wargears/Create
        public ActionResult Create()
        {
            ViewBag.WargearslotId = new SelectList(db.WargearSlots, "Id", "Name");
            return View();
        }

        // POST: Wargears/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name,Description,Image,WargearSlotId,Points,Strength,AP,Damage,Range,Type,Abilities")] Wargear wargear)
        {
            if (ModelState.IsValid)
            {
                db.Wargears.Add(wargear);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.WargearslotId = new SelectList(db.WargearSlots, "Id", "Name", wargear.WargearSlotId);
            return View(wargear);
        }

        // GET: Wargears/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Wargear wargear = db.Wargears.Find(id);
            if (wargear == null)
            {
                return HttpNotFound();
            }
            ViewBag.WargearslotId = new SelectList(db.WargearSlots, "Id", "Name", wargear.WargearSlotId);
            return View(wargear);
        }

        // POST: Wargears/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Description,Image,WargearSlotId,Points,Strength,AP,Damage,Range,Type,Abilities")] Wargear wargear)
        {
            if (ModelState.IsValid)
            {
                db.Entry(wargear).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.WargearslotId = new SelectList(db.WargearSlots, "Id", "Name", wargear.WargearSlotId);
            return View(wargear);
        }

        // GET: Wargears/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Wargear wargear = db.Wargears.Find(id);
            if (wargear == null)
            {
                return HttpNotFound();
            }
            return View(wargear);
        }

        // POST: Wargears/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Wargear wargear = db.Wargears.Find(id);
            db.Wargears.Remove(wargear);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

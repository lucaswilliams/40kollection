﻿using Kollection.Model.Enums;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Kollection
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalFilters.Filters.Add(new UserLevelActionFilter(), 0);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new KollectionViewEngine());
        }
    }

    public class UserLevelActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            UserLevel userLevel = UserLevel.NotLoggedIn;
            IPrincipal user = HttpContext.Current.User;

            if (user != null && user.Identity.IsAuthenticated)
            {
                
                //User is logged in, get their role.
                if (user.IsInRole("Freebie"))
                {
                    userLevel = UserLevel.Freebie;
                }

                if (user.IsInRole("Subscriber"))
                {
                    userLevel = UserLevel.Subscriber;
                }

                if (user.IsInRole("Admin"))
                {
                    userLevel = UserLevel.Administrator;
                }
            }
            
            filterContext.Controller.ViewBag.UserLevel = userLevel;
            base.OnActionExecuting(filterContext);
        }
    }

    public class KollectionViewEngine : RazorViewEngine
    {
        public override ViewEngineResult FindView(ControllerContext controllerContext, string viewName, string masterName, bool useCache)
        {
            string ns = controllerContext.Controller.GetType().Namespace;
            string controller = controllerContext.Controller.GetType().Name.Replace("Controller", "");
            string baseControllerNamespace = "Kollection.Controllers";

            if (viewName == "Create" || viewName == "Edit")
            {
                controllerContext.Controller.ViewBag.AddEdit = viewName;
                viewName = "Form";
            }

            controllerContext.Controller.ViewBag.ShowAll = false;
            if (viewName == "ShowAll")
            {
                controllerContext.Controller.ViewBag.ShowAll = true;
                viewName = "Index";
            }

            //try to find the view            
            string rel = "~/Views/" + (ns == baseControllerNamespace ? "" : ns.Substring(baseControllerNamespace.Length + 1).Replace(".", "/") + "/") + controller;
            string[] pathsToSearch = { rel + "/" + viewName + ".cshtml" };

            string viewPath = null;
            foreach (var path in pathsToSearch)
            {
                if (this.VirtualPathProvider.FileExists(path))
                {
                    viewPath = path;
                    break;
                }
            }

            if (viewPath != null)
            {
                string masterPath = null;

                //try find the master
                if (!string.IsNullOrEmpty(masterName))
                {
                    string[] masterPathsToSearch = new string[]{
                        rel + "/" + masterName + ".master",
                        "~/Views/" + controller + "/" + masterName + ".master",
                        "~/Views/Shared/" + masterName + ".master"
                    };

                    foreach (var path in masterPathsToSearch)
                    {
                        if (this.VirtualPathProvider.FileExists(path))
                        {
                            masterPath = path;
                            break;
                        }
                    }
                }

                if (string.IsNullOrEmpty(masterName) || masterPath != null)
                {
                    return new ViewEngineResult(
                        this.CreateView(controllerContext, viewPath, masterPath), this);
                }
            }

            //try default implementation
            var result = base.FindView(controllerContext, viewName, masterName, useCache);
            if (result.View == null)
            {
                //add the location searched
                return new ViewEngineResult(pathsToSearch);
            }
            return result;
        }
    }
}

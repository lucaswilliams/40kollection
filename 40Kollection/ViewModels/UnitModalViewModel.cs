﻿using System.Collections.Generic;
using Kollection.Model.Entities;

namespace Kollection.ViewModels
{
    public class UnitModalViewModel
    {
        public Unit Unit { get; set; }
        public List<UnitModalWargearSlotViewModel> WargearSlots { get; set; }
    }

    public class UnitModalWargearSlotViewModel
    {
        public string Name { get; set; }
        public List<Wargear> Wargears { get; set; }
    }
}
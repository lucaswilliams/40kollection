﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Kollection.ViewModels
{
    public class CollectionDetailsViewModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        [DisplayFormat(NullDisplayText = "http://placehold.it/300x300")]
        public string Image { get; set; }

        //Units in the collection.
        public List<CollectionDetailsUnitViewModel> Units { get; set; }
    }

    public class CollectionDetailsUnitViewModel
    {
        public string Name { get; set; }
        public string Wargear { get; set; }
        public int Points { get; set; }
        public int Quantity { get; set; }
    }
}
﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Kollection.ViewModels
{
    public class SquadViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        [DisplayFormat(NullDisplayText = "http://placehold.it/300x300")]
        public string Image { get; set; }
        public int Points { get; set; }
        [Display(Name = "Squad Role")]
        public int SquadRoleId { get; set; }
        
        //Linked to a user
        public string UserId { get; set; }
        
        [Display(Name = "Part of Collection")]
        public int CollectionId { get; set; }
        
        public List<UnitWargearViewModel> Units { get; set; }
    }
}
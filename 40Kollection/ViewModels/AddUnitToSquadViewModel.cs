﻿using System.Collections.Generic;

namespace Kollection.ViewModels
{
    public class AddUnitToSquadViewModel
    {
        public string UnitName { get; set; }   
        public List<UnitWargearViewModel> UnitGear { get; set; }     
    }

    public class UnitWargearViewModel
    {
        public int CollectionUnitId { get; set; }
        public string UnitName { get; set; }
        public string UnitWargear { get; set; }
    }
}